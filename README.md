# Azure PathFinder API

A repository for the API service that powers Azure PathFinder

## Functions

This section provides an overview of the Azure functions, and their signatures.

### AirportValidator

Takes a list of IATA airport codes, and ensures they are present in our dataset.

#### Inputs

* `airports` query parameter OR request body  
  This parameter must contain a JSON list of strings, each containing an IATA airport code.  
  Example:
  ```JSON
  ["PDX", "SEA"]
  ```  
  *NOTE*: The query parameter takes priority, if both values are set, only the query parameter will be evaluated.

#### Outputs

* `HTTP 400 Bad Request` in any of the following scenarios:
  * Neither the query parameter or request body were set.
  * The input could not be deserialized into a list of strings.
  * The list resulting from deseriailization is empty.  
  In any of these scenarios, the response body will contain a message indicating what was wrong.
* `HTTP 404 Not Found` if any of the provided IATA codes are not contained within our dataset.
  The response body will contain a JSON list of any provided codes that could not be found.
* `HTTP 200 OK` if all of the provided IATA codes are contained within our dataset.  
  No response body is provided.

## Development

### Prerequisits

* Access to an Azure subscription (We're using the "Portland Azure Innovation Lab").
* [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli) installed.
* Azure CLI logged in - use `az login`.


### Deployment

To deploy the [Bicep](https://docs.microsoft.com/en-us/azure/azure-resource-manager/bicep/) templates run:


```Bicep
az deployment sub create --location westus2 --template-file main.bicep --parameters @main.parameters.json
```

This will deploy the main bicep file, which in turn calls the other resource templates. Parameters are populated by the `main.parameters.json` file.

Then change into the `functions` directory and deploy the Azure Functions:

```Bash
cd functions && func azure functionapp publish azure-pathfinder-api
```

### JSON Schema

Each JSON file has a schema specification written. If you wish to test that your JSON files are valid, do the following:

1. Install [jsonschema](https://github.com/Julian/jsonschema) via pip - Eg. `pip3 install jsonschema`
2. Run the validator against your json file - Eg. `jsonschema -i airports.json airports.schema.json`
If the output is empty, your JSON file is valid.

## Notes

* [Nested Bicep / ARM Templates don't support compelte mode](https://github.com/Azure/bicep/issues/1367) - Meaning removing a resource from a nested template and re-deploy does *not* automatically remove it.
* Local development required me to hardcode the storage access key - Is there a way to avoid this?
* I don't like that deploying the infrastructure and the Azure Function code is two separate steps - Can this be improved?
* I'm hardcoding the name of the airports data blob in the AirportsValidator function - This should be a parameter if possible.
