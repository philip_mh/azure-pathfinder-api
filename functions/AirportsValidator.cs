using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

using ErrorEventArgs = Newtonsoft.Json.Serialization.ErrorEventArgs;

namespace functions
{
    public static class AirportsValidator
    {
        [FunctionName("AirportsValidator")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post")] HttpRequest request,
            [Blob("data/airports.json", FileAccess.Read)] string airportsBlob,
            ILogger log
        )
        {
            log.LogInformation("AirportsValidator was triggered.");
            log.LogInformation($"Blob loaded with content: {airportsBlob}");
            Dictionary<string, Object> airportsData = JsonConvert.DeserializeObject<Dictionary<string, Object>>(airportsBlob);
            log.LogInformation($"Airport list contains: {String.Join(", ", airportsData.Keys.ToArray())}");

            string queryParam = request.Query["airports"];
            log.LogInformation($"Query parameter 'airports' contains: {queryParam}");

            string requestBody = await new StreamReader(request.Body).ReadToEndAsync();
            log.LogInformation($"Request body contains: {requestBody}");

            string inputString = queryParam ?? requestBody;
            log.LogInformation($"inputstring contains: {inputString}");

            if (String.IsNullOrWhiteSpace(inputString)) {
                string message = "Input is empty!";
                log.LogInformation(message);
                return new BadRequestObjectResult(message);
            }

            List<string> inputList = new List<string>();
            List<string> errors = new List<string>();
            JsonSerializerSettings serializerSettings = new JsonSerializerSettings
            {
                Error = delegate(object sender, ErrorEventArgs args)
                {
                    errors.Add(args.ErrorContext.Error.Message);
                    args.ErrorContext.Handled = true;
                }
            };

            inputList = JsonConvert.DeserializeObject<List<string>>(inputString, serializerSettings);
            if (errors.Any()) {
                log.LogInformation("Errors ocurred during input parsing:");
                log.LogInformation(String.Join("\n", errors.ToArray()));
                return new BadRequestObjectResult(String.Join("\n", errors.ToArray()));
            }

            log.LogInformation($"Input list contains: {String.Join(", ", inputList.ToArray())}");

            if (!inputList.Any()) {
                string message = "Input list is empty!";
                log.LogInformation(message);
                return new BadRequestObjectResult(message);
            }

            List<string> missing = inputList.ConvertAll(s => s.ToUpper()).Except(airportsData.Keys.ToList().ConvertAll(s => s.ToUpper())).ToList();
            log.LogInformation($"Input items not found in airports list: {String.Join(", ", missing.ToArray())}");

            if (missing.Any()) {
                return new NotFoundObjectResult(JsonConvert.SerializeObject(missing));
            }
            return new OkResult();
        }
    }
}
