@maxLength(24)
@minLength(3)
@description('The name of the storage account in which the data will be stored. Most be globally unique across Azure.')
param name string

@description('The UTC timestamp of the deployment, used to ensure each deployment generates a unique deployment script.')
param utc string = utcNow()

var fileName = 'airports.json'
var containerName = 'data'

resource storageAccount 'Microsoft.Storage/storageAccounts@2021-06-01' = {
  name: name
  location: resourceGroup().location
  sku: {
    name: 'Standard_LRS'
  }
  kind: 'StorageV2'
  tags: resourceGroup().tags

  resource blobService 'blobServices@2021-06-01' = {
    name: 'default'
    resource container 'containers' = {
      name: containerName
      properties: {
        publicAccess: 'None'
      }
    }
  }
}

resource upload 'Microsoft.Resources/deploymentScripts@2020-10-01' = {
  name: 'upload-blob-${utc}'
  location: resourceGroup().location
  kind: 'AzureCLI'
  properties: {
    azCliVersion: '2.32.0'
    retentionInterval: 'P1D'
    timeout: 'PT10M'
    scriptContent: 'echo $CONTENT > ${fileName} && az storage blob upload -f ${fileName} -c ${containerName} -n ${fileName}'
    environmentVariables: [
      {
        name: 'CONTENT'
        value: loadTextContent(fileName)
      }
      {
        name: 'AZURE_STORAGE_ACCOUNT'
        value: storageAccount.name
      }
      {
        name: 'AZURE_STORAGE_KEY'
        value: storageAccount.listKeys().keys[0].value
      }
    ]
  }
  tags: resourceGroup().tags
}

output storageAccountId string = storageAccount.id
output storageAccountName string = storageAccount.name
output storageAccountApiVersion string = storageAccount.apiVersion
