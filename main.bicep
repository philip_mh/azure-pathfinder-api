targetScope = 'subscription'

param appName string
param storageName string
param location string
param tags object
param appContacts array
param targetCost int

resource rg 'Microsoft.Resources/resourceGroups@2021-04-01' = {
  name: appName
  location: location
  tags: tags
}

module budgetModule 'budget.bicep' = {
  scope: rg
  name:  'budgetDeploy'
  params: {
    contacts: appContacts
    name: appName
    targetCost: targetCost
  }
}

module storageModule 'storage.bicep' = {
  scope: rg
  name: 'storageDeploy'
  params: {
    name: storageName
  }
}

module functionModule 'function.bicep' = {
  scope: rg
  name: 'functionDeploy'
  params: {
    name: appName
    storageAccountId: storageModule.outputs.storageAccountId
    storageAccountName: storageModule.outputs.storageAccountName
    storageAccountApiVersion: storageModule.outputs.storageAccountApiVersion
  }
}
