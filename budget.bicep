@minLength(1)
@maxLength(63)
param name string
param contacts array
param targetCost int

resource budget 'Microsoft.Consumption/budgets@2021-10-01' = {
  name: name
  properties: {
    category: 'Cost'
    amount: targetCost
    timeGrain: 'Monthly'
    timePeriod: {
      startDate: '2022-01-01'
    }
    notifications: {
      actual50: {
        operator: 'GreaterThanOrEqualTo'
        threshold: 50
        contactEmails: contacts
        enabled: true
        thresholdType: 'Actual'
      }
      actual75: {
        operator: 'GreaterThanOrEqualTo'
        threshold: 75
        contactEmails: contacts
        enabled: true
        thresholdType: 'Actual'
      }
      actual100: {
        operator: 'GreaterThanOrEqualTo'
        threshold: 100
        contactEmails: contacts
        enabled: true
        thresholdType: 'Actual'
      }
      forecast50: {
        operator: 'GreaterThanOrEqualTo'
        threshold: 50
        contactEmails: contacts
        enabled: true
        thresholdType: 'Forecasted'
      }
      forecast75: {
        operator: 'GreaterThanOrEqualTo'
        threshold: 75
        contactEmails: contacts
        enabled: true
        thresholdType: 'Forecasted'
      }
      forecast100: {
        operator: 'GreaterThanOrEqualTo'
        threshold: 100
        contactEmails: contacts
        enabled: true
        thresholdType: 'Forecasted'
      }
    }
  }
}
